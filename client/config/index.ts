export default {
  API_URL: import.meta.env.VITE_API_URL,
  ACCESS_TOKEN_KEY: 'access_token',
};
